sealed class Country {
    object USA : Country()
}

object Spain : Country()
class Greece(val someProperty: String) : Country()
data class Canada(val someProperty: String) : Country()

class Currency(
    val code: String
)

object CurrencyFactory {

    fun currencyForCountry(country: Country): Currency =
        when (country) {
            is Greece -> Currency("EUR")
            is Spain -> Currency("EUR")
            is Country.USA -> Currency("USD")
            is Canada -> Currency("CAD")
        }
}

fun main(args: Array<String>)
{
    val greeceCurrency = CurrencyFactory.currencyForCountry(Greece("")).code
    println("Moneda de Grecia: $greeceCurrency")

    val usaCurrency = CurrencyFactory.currencyForCountry(Country.USA).code
    println("Moneda Estadounidense: $usaCurrency")

    val cadCurrency = CurrencyFactory.currencyForCountry(Canada("")).code
    println("Moneda Canadiense: $cadCurrency")
}